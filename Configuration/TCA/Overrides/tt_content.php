<?php
defined('TYPO3_MODE') || die();

(function($table) {

    // Get extension configuration
    $confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['cewrap']);
    $l10n_mode = ($confArr['l10n_mode_exclude_not'] ? '' : 'exclude');

    // Extra fields for the tt_content table
    $newContentColumns = [
        'tx_cewrap_active' => [
            'exclude' => 1,
            'l10n_mode' => $l10n_mode,
            'label' => 'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_active',
            'onChange' => 'reload',
            'config' => [
                'type' => 'check',
            ],
        ],
        'tx_cewrap_id_input' => [
            'exclude' => 1,
            'l10n_mode' => $l10n_mode,
            'label' => 'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_id_input',
            'displayCond' => 'FIELD:tx_cewrap_active:REQ:true',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim,nospace',
            ],
        ],
        'tx_cewrap_type' => [
            'exclude' => 1,
            'l10n_mode' => $l10n_mode,
            'label' => 'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_type',
            'displayCond' => 'FIELD:tx_cewrap_active:REQ:true',
            'config' => [
                'type' => 'check',
            ],
        ],
        'tx_cewrap_class_input' => [
            'exclude' => 1,
            'l10n_mode' => $l10n_mode,
            'label' => 'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_input',
            'displayCond' => 'FIELD:tx_cewrap_active:REQ:true',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
            ],
        ],
        'tx_cewrap_class_select' => [
            'exclude' => 1,
            'l10n_mode' => $l10n_mode,
            'label' => 'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_select',
            'displayCond' => 'FIELD:tx_cewrap_active:REQ:true',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'items' => [
                    [
                        'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_select.ruler_before',
                        'frame-ruler-before'
                    ],
                    [
                        'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_select.ruler_after',
                        'frame-ruler-after'
                    ],
                    [
                        'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_select.box',
                        'frame-box'
                    ],
                    [
                        'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_select.hidden-xs',
                        'hidden-xs'
                    ],
                    [
                        'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_select.hidden-sm',
                        'hidden-sm'
                    ],
                    [
                        'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_select.hidden-md',
                        'hidden-md'
                    ],
                    [
                        'LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:tt_content.tx_cewrap_class_select.hidden-lg',
                        'hidden-lg'
                    ]
                ]
            ],
        ],
    ];

    // Adding fields to the tt_content table definition in TCA
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns($table, $newContentColumns);

    // Create palette
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes($table, '--palette--;LLL:EXT:cewrap/Resources/Private/Language/locallang.xlf:palette.cewrap;cewrap', '', 'after:layout');
    $GLOBALS['TCA'][$table]['palettes']['cewrap']['showitem'] = 'tx_cewrap_active, --linebreak--, tx_cewrap_id_input, tx_cewrap_type, --linebreak--, tx_cewrap_class_input, tx_cewrap_class_select';

})('tt_content');
