<?php
defined('TYPO3_MODE') || die('Access denied.');

// Register Page TSconfig for EXT:cefooter
if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('cefooter')) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
        'cewrap',
        'Configuration/TypoScript/PageTS/mod.typoscript',
        'EXT:cefooter: Preview settings for EXT:cewrap'
    );
}