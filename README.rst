=============
Documentation
=============

----------------
What does it do?
----------------

This extension allows wrapping of all content elements with id (uid of content element or your own id) and/or class (type of content element, or your own class, or selection of predefined classes). It does the same as the selectbox "frame_class" (wrap a content element in a div container), but allows you to type in the name for the id or class attribute or select multiple predefined classes, so the results are s.th. like this:

::

    <div id="c123" class="my-class">...</div>
    <div id="my-id">...</div>
    <div class="frame type-text my-second-class ruler-before ruler-after hidden-xs">...</div>

This feature isn't mainly intended for editors, but for developers. It allows you easily to address a specific content element by CSS or JavaScript.


Predefined classes
==================

The selection of predefined classes is very useful, also for editors, e.g. to hide content elements dependent to screen width. Predefined classes are:

- frame-ruler-before: Set horizontal line before this element
- frame-ruler-after: Set horizontal line after this element
- frame-box: Box with background
- hidden-xs: Hide element on extra small devices (mobile)
- hidden-sm: Hide element on small devices (tablet)
- hidden-md: Hide element on medium devices (desktop)
- hidden-lg: Hide element on large devices (desktop)

The predefined classes can easily be removed and new classes can be added, see chapter "Configuration of CSS classes".


-----------
Screenshots
-----------

Input fields and checkboxes for id and / or class of a content element in the backend:

.. figure:: Documentation/screenshot1.png

    This extension doesn't flood your BE form with many fields: The checkbox and two inputfields are only visible if you check the checkbox "Activate?".


Preview of settings in footer of tt_content element (use EXT:cefooter for this):

.. figure:: Documentation/screenshot2.png


-------------
Configuration
-------------


Add TypoScript template
=======================

Add the template "Content element wrap (cewrap)" in field "Include static (from extensions)" in your ROOT page on **last** position.
Last position because it overwrites Fluid Templates and TypoScript settings which are sometimes overwritten by other extensions.
At least, you should add this template **after** the static templates of EXT:bootstrap_package and EXT:fluid_styled_content.


Add Page TSconfig
=================

If you have installed EXT:cefooter, add the predefined Page TSconfig by selecting item "EXT:cefooter: Preview settings for EXT:cewrap" in your ROOT page or write your own Page TSconfig.


Configuration of CSS classes
============================

Configure the CSS classes an editor can choose from. Configuration is possible with TCA or in Page TSconfig.

Example for configuration in Page TSconfig:

::

    TCEFORM.tt_content.tx_cewrap_class_select {
        removeItems := addToList(frame-box)
        addItems {
            frame-mybox = LLL:EXT:myext/Resources/Private/Language/backend.xlf:tt_content.tx_cewrap_class_select.mybox
        }
        types {
            image.addItems {
                frame-slider = LLL:EXT:myext/Resources/Private/Language/backend.xlf:tt_content.tx_cewrap_class_select.slider
            }
        }
    }


Clean up your HTML code
=======================

For a clean HTML code you should set the default value for field "Frame" to "No Frame" instead of "Default". You do that with this TypoScript configuration in Page TSConfig:

::

    TCAdefaults.tt_content.frame_class = none

This prevents that all content elements are wrapped in a div with id (you can control this behaviour with this extension, if you need a content element to be wrapped).


---
FAQ
---

I want to wrap ALL content elements
===================================

If you want to wrap **every** content element with a class, it's id and/or the content element type, don't use this extension. This can be accomplished with TypoScript or Fluid (depends on your TYPO3 setup and the distributions/extensions you use).

Usage with TYPO3 < 10
=====================

Please use version 4.3.1 from TER.

Usage with TYPO3 < 8
====================

Please use version 3.1.0 from TER.


Usage with TYPO3 < 7
====================

Please use version 2.2.0 from TER.


Usage with TYPO3 < 6.2
======================

Have a look into TER for older versions of this extension.


Usage with EXT:css_styled_content
=================================

Please use version 3.1.0 (or older) from TER.


---------
ChangeLog
---------

See file **ChangeLog** in the extension directory.
