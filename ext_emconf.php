<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "cewrap".
 *
 * Auto generated 25-05-2020 07:58
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'Content element wrap',
  'description' => 'Allows to wrap content elements with individual IDs and/or classes and select multiple predefined classes, e.g. to hide content elements dependent to screen width.',
  'category' => 'fe',
  'version' => '4.4.0',
  'state' => 'stable',
  'uploadfolder' => false,
  'clearcacheonload' => true,
  'author' => 'Sven Burkert',
  'author_email' => 'bedienung@sbtheke.de',
  'author_company' => 'SBTheke web development',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '10.4.0-10.4.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
      'cefooter' => '',
      'fluid_styled_content' => '',
      'bootstrap_package' => '11.0.0-',
      'gridelements' => '',
    ),
  ),
);

