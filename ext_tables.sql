#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
	tx_cewrap_active tinyint(1) DEFAULT '0' NOT NULL,
	tx_cewrap_type tinyint(1) DEFAULT '0' NOT NULL,
	tx_cewrap_id_input varchar(255) DEFAULT '' NOT NULL,
	tx_cewrap_class_input varchar(255) DEFAULT '' NOT NULL,
	tx_cewrap_class_select text
);
