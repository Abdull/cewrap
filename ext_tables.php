<?php
defined('TYPO3_MODE') || die('Access denied.');

// Add static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('cewrap', 'Configuration/TypoScript/', 'Content element wrap');

// Registering CSH (Context Sensitive Help)
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tt_content', 'EXT:cewrap/Resources/Private/Language/locallang_csh.xlf');
